from django.db import models

class Urls_DataBase(models.Model):
    # Parameters of the Short Url
    url = models.TextField()
    shortURL = models.CharField(max_length=64)

    # String of the Short Url
    def __str__(self):
        return self.shortURL

