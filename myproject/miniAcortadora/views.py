from django.shortcuts import render, redirect
from .models import Urls_DataBase
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def get_counter():
    counter = 1
    is_here = False
    if Urls_DataBase.objects.count() != 0:
        while not is_here:
            search_url = Urls_DataBase.objects.filter(short=str(counter))
            if search_url:
                counter += 1
            else:
                is_here = True
    return counter

def get_resource(request, resource):
    try:
        short_url = Urls_DataBase.objects.get(short=resource)
        return redirect(short_url.url)
    except Urls_DataBase.DoesNotExist:
        url_dic = Urls_DataBase.objects.all()
        return render(request, "shortener/Error.html", {'resource':resource, 'url_dic': url_dic})

#Get the information from the form and save it into the database
@csrf_exempt
def index(request):
    if request.method == "GET":
        url_dic = Urls_DataBase.objects.all()
        return render(request, "shortener/Page.html", {'url_dic': url_dic})
    elif request.method == "POST":
        url_dic = Urls_DataBase.objects.all()
        sh_url = request.POST["url"]
        url = Urls_DataBase.objects.filter(url__endswith=sh_url)
        if not url:
            if 'http://' not in sh_url and 'https://' not in sh_url:
                sh_url = 'http://' + sh_url
            if not request.POST["short"]:
                counter = get_counter()
                url = Urls_DataBase(short=str(counter), url=sh_url)
                url.save()
            else:
                url = Urls_DataBase(short=request.POST["short"], url=sh_url)
                url.save()
        return render(request, "shortener/Page.html", {'url_dic': url_dic})

